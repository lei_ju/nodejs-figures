%{?nodejs_find_provides_and_requires}
%global packagename figures
%global enable_tests 0
Name:		nodejs-figures
Version:	1.7.0
Release:	1
Summary:	Unicode symbols with Windows CMD fallbacks
License:	MIT
URL:		https://github.com/sindresorhus/figures.git
Source0:	https://registry.npmjs.org/figures/-/figures-%{version}.tgz
Source1:	https://raw.githubusercontent.com/sindresorhus/figures/v%{version}/test.js
ExclusiveArch:	%{nodejs_arches} noarch
BuildArch:	noarch
BuildRequires:       	nodejs-packaging 	npm(object-assign) 	npm(escape-string-regexp)
%if 0%{?enable_tests}
BuildRequires:       	npm(ava)
%endif

%description
Unicode symbols with Windows CMD fallbacks

%prep
%setup -q -n package
cp -p %{SOURCE1} .

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{packagename}
cp -pr package.json index.js \
	%{buildroot}%{nodejs_sitelib}/%{packagename}
%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
%if 0%{?enable_tests}
%{_bindir}/ava
%endif

%files
%{!?_licensedir:%global license %doc}
%doc *.md
%license license
%{nodejs_sitelib}/%{packagename}

%changelog
* Tue Aug 18 2020 leiju <leiju4@huawei.com> - 1.7.0-1
- Package init
